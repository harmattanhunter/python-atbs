# Game inventory
stuff = {'rope': 1, 'torch': 6, 'gold coin': 42, 'dagger': 1, 'arrow': 12}
dragonLoot = ['gold coin', 'dagger', 'gold coin', 'gold coin', 'ruby','arrow','arrow','kashMag']

def addToInventory(inv, ls):
    for item in ls:
        if (item in inv):
            inv[item] = inv[item] + 1
        else:
            inv[item]= 1
    
    print('\nUpdated Inventory')
    displayInventory(inv)

def displayInventory(inv):
    sum = 0
    print("Inventory")

    for k, v in inv.items():
        print(v, end=" ")
        print(k)
        sum += v

    print('Total items: ' + str(sum))

displayInventory(stuff)
addToInventory(stuff, dragonLoot)

fred =(1,2,3,4)
print(fred[2])