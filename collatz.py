# Python program simulating the collatz sequence
def collatz(number):
    if (number%2==0):
        return number//2
    else:
        return 3 * number + 1

print("Enter an integer: ", end='')

try:
    userChoice = int(input())
except ValueError:
    print("You should enter an integer")

print(userChoice)

while (userChoice!=1):
    userChoice = collatz(userChoice)
    print(userChoice)
