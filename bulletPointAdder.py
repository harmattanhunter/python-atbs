#! /usr/bin/python3

## add bullet points to a list from wikipedia

import pyperclip


def turnToBullet(txt):
    txtString = txt.split(', ')
    
    for i in range(len(txtString)):
        txtString[i] = "* " + txtString[i].title()

    return txtString

text = pyperclip.paste()

print(text)

bulletList = turnToBullet(text)

for i in bulletList:
    print(i)
