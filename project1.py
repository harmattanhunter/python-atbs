#! /usr/bin/python3

# project1.py - a frankly useless password manager

PASSWORDS = {'yahoo':'xxyyXXX', 'email':'resioonpeere@x3#', 'messenger':'seinc23334xx'}

import pyperclip, sys


if len(sys.argv)< 2:
    print('Usage: ./project1.py [account]')
    sys.exit()

account = sys.argv[1]

if account in PASSWORDS:
    pyperclip.copy(PASSWORDS[account])
    print('Passsword copied to clipboard')
else:
    print('Account not found. Enter valid account')
    sys.exit()

